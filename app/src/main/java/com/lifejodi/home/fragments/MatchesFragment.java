package com.lifejodi.home.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lifejodi.R;
import com.lifejodi.home.adapters.MatchesRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Administrator on 1/27/2018.
 */

public class MatchesFragment extends Fragment {

    @BindView(R.id.rec_fragment_matches)
    RecyclerView recFragmentMatches;
    Unbinder unbinder;

    GridLayoutManager gridLayoutManager;
    MatchesRecyclerAdapter matchesRecyclerAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matches, null);
        unbinder = ButterKnife.bind(this, view);
        gridLayoutManager = new GridLayoutManager(getActivity(),2);
        recFragmentMatches.setLayoutManager(gridLayoutManager);
        matchesRecyclerAdapter = new MatchesRecyclerAdapter(getActivity());
        recFragmentMatches.setAdapter(matchesRecyclerAdapter);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
