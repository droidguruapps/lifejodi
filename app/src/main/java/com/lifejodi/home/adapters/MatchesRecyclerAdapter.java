package com.lifejodi.home.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lifejodi.R;
import com.lifejodi.home.activity.ProfileListActivity;

/**
 * Created by Administrator on 1/27/2018.
 */

public class MatchesRecyclerAdapter extends RecyclerView.Adapter<MatchesRecyclerAdapter.MatchesHolder> {

    Context context;
    public MatchesRecyclerAdapter(Context con)
    {
        context =con;
    }
    @Override
    public MatchesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_fragment_matches,parent,false);
        MatchesHolder matchesHolder = new MatchesHolder(view);
        return matchesHolder;
    }

    @Override
    public void onBindViewHolder(MatchesHolder holder, int position) {

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProfileListActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MatchesHolder extends RecyclerView.ViewHolder
    {
        LinearLayout layout;
        public MatchesHolder(View itemView) {
            super(itemView);
            layout = (LinearLayout)itemView.findViewById(R.id.layout_home_list);
        }
    }
}
