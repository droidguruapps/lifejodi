package com.lifejodi.home.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.lifejodi.event.activity.EventsActivity;
import com.lifejodi.InboxActivity;
import com.lifejodi.NotificationActivity;
import com.lifejodi.ProfileActivity;
import com.lifejodi.R;
import com.lifejodi.SearchActivity;
import com.lifejodi.home.adapters.HomeViewPagerAdapter;
import com.lifejodi.utils.AppController;
import com.lifejodi.utils.Constants;
import com.lifejodi.utils.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ajay on 11-11-2017.
 */

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    TextView tvHeaderName;
    SharedPreference sharedPreference = SharedPreference.getSharedInstance();

    AppController appController = AppController.getInstance();
    HomeViewPagerAdapter homeViewPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        initialization();
    }

    private void initialization() {
        sharedPreference.initialize(this);
        appController.initialize(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this);

        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        bottomNavigation.setSelectedItemId(R.id.navigation_home);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.setTabTextColors(Color.parseColor("#FFDCDADA"), Color.parseColor("#ffffff"));
        tabs.addTab(tabs.newTab().setText("MATCHES"));
        tabs.addTab(tabs.newTab().setText("NEW MATCHES"));
        tabs.addTab(tabs.newTab().setText("SHORTLISTED"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        View view = navView.getHeaderView(0);
        tvHeaderName = (TextView)view.findViewById(R.id.nav_header_username);
        tvHeaderName.setText(sharedPreference.getSharedPrefData(Constants.USERNAME));

        homeViewPagerAdapter = new HomeViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(homeViewPagerAdapter);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_my_matches) {
            // Handle the camera action
        } else if (id == R.id.nav_inbox) {
            startActivity(new Intent(getApplicationContext(),InboxActivity.class));
        } else if (id == R.id.nav_upgrade_account) {

        } else if (id == R.id.nav_chat) {

        } else if (id == R.id.nav_edit_profile) {
            startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
        } else if (id == R.id.nav_setting) {

        } else if (id == R.id.nav_daily_recomm) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    return true;
                case R.id.navigation_mail:
                    startActivity(new Intent(getApplicationContext(),InboxActivity.class));
                    return true;
                case R.id.navigation_notifications:
                    startActivity(new Intent(getApplicationContext(),NotificationActivity.class));
                    return true;
                case R.id.navigation_search:
                    startActivity(new Intent(getApplicationContext(),SearchActivity.class));
                    return true;
                case R.id.navigation_events:
                    startActivity(new Intent(getApplicationContext(),EventsActivity.class));
                    return true;
            }
            return false;
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            appController.doubleTapToExit(HomeActivity.this);
        }
        return true;
    }
}
